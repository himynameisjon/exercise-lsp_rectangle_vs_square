<?php
namespace Shape;

use Shape\Exception\RectangleException;

class Rectangle
{
    private $topLeft;
    private $width;
    private $height;

    public function getHeight()
    {
        return $this->height;
    }

    public function setHeight($height)
    {
        $this->height = $height;
    }

    public function getWidth()
    {
        return $this->width;
    }

    public function setWidth($width)
    {
        $this->width = $width;
    }

    public function area()
    {
        if (!$this->width || !$this->height) {
            throw new RectangleException();
        }

        return $this->width * $this->height;
    }
}
