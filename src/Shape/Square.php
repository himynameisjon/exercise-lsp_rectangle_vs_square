<?php
namespace Shape;

use Shape\Exception\SquareException;

class Square extends Rectangle
{
    public function setHeight($value)
    {
        $this->width = $value;
        $this->height = $value;
    }

    public function setWidth($value)
    {
        $this->width = $value;
        $this->height = $value;
    }

    public function area()
    {
        if (!$this->width || !$this->height) {
            throw SquareException;
        }

        return $this->width * $this->height;
    }
}
