# Instructions

1. Identify violations of the Liskov substitution principle.
2. Refactor the code to remove any violations.

```
<?php
class RectangleException extends Exception {}

class Rectangle
{
    private $topLeft;
    private $width;
    private $height;

    public function getHeight()
    {
        return $this->height;
    }

    public function setHeight($height)
    {
        $this->height = $height;
    }

    public function getWidth()
    {
        return $this->width;
    }

    public function setWidth($width)
    {
        $this->width = $width;
    }

    public function area()
    {
        if (!$this->width || !$this->height) {
            throw new RectangleException();
        }

        return $this->width * $this->height;
    }
}

class SquareException extends Exception {}

class Square extends Rectangle
{
    public function setHeight($value)
    {
        $this->width = $value;
        $this->height = $value;
    }

    public function setWidth($value)
    {
        $this->width = $value;
        $this->height = $value;
    }

    public function area()
    {
        if (!$this->width || !$this->height) {
            throw new SquareException();
        }

        return $this->width * $this->height;
    }
}

class Client
{
    public function areaVerifier(Rectangle $r)
    {
        $r->setWidth(5);
        $r->setHeight(4);

        if ($r->area() != 20) {
            throw new Exception('Bad area!');
        }

        return true;
    }
}
```
